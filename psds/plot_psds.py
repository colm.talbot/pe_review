import glob

import matplotlib.pyplot as plt
import numpy as np

psd_files = glob.glob("*.txt")

for psd_file in psd_files:
    print(psd_file)
    label = psd_file.split("/")[-1][:-4].replace("_", " ").title()
    frequencies, psd = np.genfromtxt(psd_file).T
    plt.loglog(frequencies, psd, label=label)
plt.xlabel("Frequency [Hz]")
plt.ylabel("PSD [Hz$^{-1}$]")
plt.legend(loc="upper right")
plt.tight_layout()
plt.savefig("psds.pdf")
plt.close()

