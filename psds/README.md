PSD files taken from [T2000012](https://dcc.ligo.org/LIGO-T2000012-v1/public).

For the simulations we take the real O3 PSDs.

Beware, this directory contains ASDs and PSDs.
