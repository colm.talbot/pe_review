# PE review data

Code and data used to generate PE review data for O4.

## Data generation

The main script `create_single_frame.py` is based heavily on [this script](https://git.ligo.org/pe/O4/o4_online_pe_mdc/-/tree/master/results).

This script creates three 1164s frame files (LIGO Hanford, LIGO Livingston, and Virgo), each of which has an injection 32s before the end of the frame.
Signals are generated from 10 Hz with a sampling frequency of 16 kHz and with a reference frequency of 20 Hz.
The signal parameters are drawn from the provided prior distribution and miscalibrated according to the provided calibration envelopes.
All of the information required to describe the injected signal are stored in `meta.json`.

## PSD generation

The duration of data being analyzed impacts the PSD that should be used (see, [here](https://arxiv.org/abs/2106.13785)).
The `make_psds.py` script will generate the PSD for a given signal duration.
