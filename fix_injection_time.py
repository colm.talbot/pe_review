import glob
import json
import sys

import numpy as np


def fix_trigger_time(fname):
    with open(fname, "r") as ff:
        data = json.load(ff)
    offset = np.random.normal(0, 0.01)
    data["trigger"]["geocent"] = data["injection"]["geocent_time"] + offset
    with open(fname, "w") as ff:
        json.dump(data, ff, indent=4)


fnames = glob.glob(sys.argv[1])
for fname in fnames:
    fix_trigger_time(fname)

