#!/usr/bin/env python
import os
import argparse
import json
import subprocess
from pathlib import Path

import bilby
import numpy as np
from bilby.gw.detector import PowerSpectralDensity
import bilby_pipe
from gwpy.timeseries import TimeSeries

bilby.core.utils.setup_logger(log_level="WARNING")


def get_git_message():
    try:
        git_log = subprocess.check_output(
            ['git', 'log', '-1', '--pretty=%h %ai']).decode('utf-8')
        git_diff = (subprocess.check_output(['git', 'diff', '.']) +
                    subprocess.check_output(
                        ['git', 'diff', '--cached', '.'])).decode('utf-8')
        if git_diff == '':
            git_status = '(CLEAN) ' + git_log
        else:
            git_status = '(UNCLEAN) ' + git_log
    except subprocess.CalledProcessError:
        git_status = ""
    return git_status


parser = argparse.ArgumentParser()
parser.add_argument("--outdir", type=str, default='.')
parser.add_argument("--population", type=str, required=True)
parser.add_argument("--waveform", type=str, required=True)
parser.add_argument("--source", type=str, choices=['bbh', 'nsbh', 'bns'], required=True)
parser.add_argument("--seed", type=int, required=True)
parser.add_argument("--blind", action="store_true")
parser.add_argument("--plot", action="store_true")
parser.add_argument("--asd-dict", type=str, default=None)
parser.add_argument("--calibration-dict", type=str, default=None)
parser.add_argument("--disable-calibration", action="store_true")
parser.add_argument("--reference-time", type=float, default=1325030418)
parser.add_argument("--idx", type=int, default=None)
args = parser.parse_args()

DEFAULT_ASD_DIR = "/home/colm.talbot/O4/pe_review/psds"
DEFAULT_CAL_DIR = "/home/colm.talbot/O4/pe_review/calibration"
DEFAULT_ASDS = dict(
    H1=DEFAULT_ASD_DIR + "/aligo_O3actual_H1.txt",
    L1=DEFAULT_ASD_DIR + "/aligo_O3actual_L1.txt",
    V1=DEFAULT_ASD_DIR + "/avirgo_O3actual.txt",
)
DEFAULT_CALS = dict(
    H1=DEFAULT_CAL_DIR + "/2019-11-01_O3_LHO_GPSTime_1244415456_C01_RelativeResponseUncertainty_FinalResults.txt",
    L1=DEFAULT_CAL_DIR + "/2019-11-01_O3_LLO_GPSTime_1244415456_C01_RelativeResponseUncertainty_FinalResults.txt",
    V1=DEFAULT_CAL_DIR + "/V_O3a_calibrationUncertaintyEnvelope_magnitude5percent_phase35milliradians10microseconds.txt",
)

if args.asd_dict is None:
    asd_files = DEFAULT_ASDS
else:
    # bash has problems passing literal braces
    if args.asd_dict[0] != "{":
        args.asd_dict = "{" + args.asd_dict
    if args.asd_dict[-1] != "}":
        args.asd_dict = args.asd_dict + "}"
    asd_files = bilby_pipe.utils.convert_string_to_dict(args.asd_dict)

if args.disable_calibration:
    calibration_files = None
elif args.calibration_dict is None:
    calibration_files = DEFAULT_CALS
else:
    if args.calibration_dict[0] != "{":
        args.calibration_dict = "{" + args.calibration_dict
    if args.calibration_dict[-1] != "}":
        args.calibration_dict = args.calibration_dict + "}"
    calibration_files = bilby_pipe.utils.convert_string_to_dict(args.calibration_dict)

interferometers = asd_files.keys()

REFERENCE_FREQUENCY = 20
MINIMUM_FREQUENCY = 10
SAMPLING_FREQUENCY = 16384
MAXIMUM_FREQUENCY = SAMPLING_FREQUENCY / 2
MAX_SIGNAL = 128
TIME_AFTER_SIGNAL = 32
DURATION = 1024 + MAX_SIGNAL + TIME_AFTER_SIGNAL
TREF = args.reference_time
if args.idx is not None:
    TREF += DURATION * args.idx
THALFWINDOW = 0.1

START_TIME = TREF + TIME_AFTER_SIGNAL - DURATION
PRIOR_CLASS = dict(
    bbh=bilby.gw.prior.BBHPriorDict,
    nsbh=bilby.gw.prior.BNSPriorDict,
    bns=bilby.gw.prior.BNSPriorDict
)
MODEL = dict(
    bbh=bilby.gw.source.lal_binary_black_hole,
    nsbh=bilby.gw.source.lal_binary_neutron_star,
    bns=bilby.gw.source.lal_binary_neutron_star
)
CONVERSION = dict(
    bbh=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
    nsbh=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
    bns=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
)
GENERATION = dict(
    bbh=bilby.gw.conversion.generate_all_bbh_parameters,
    nsbh=bilby.gw.conversion.generate_all_bns_parameters,
    bns=bilby.gw.conversion.generate_all_bns_parameters,
)

np.random.seed(args.seed)

population = PRIOR_CLASS[args.source](args.population)
population["geocent_time"] = bilby.core.prior.Uniform(
    name="geocent_time",
    minimum=TREF - THALFWINDOW,
    maximum=TREF + THALFWINDOW,
)
if calibration_files is not None:
    for name in calibration_files:
        population.update(bilby.gw.prior.CalibrationPriorDict.from_envelope_file(
            envelope_file=calibration_files[name],
            minimum_frequency=MINIMUM_FREQUENCY,
            maximum_frequency=MAXIMUM_FREQUENCY,
            n_nodes=10,
            label=name,
        ))
injection_parameters = population.sample()

waveform_arguments = dict(
    waveform_approximant=args.waveform,
    reference_frequency=REFERENCE_FREQUENCY,
    minimum_frequency=MINIMUM_FREQUENCY,
    maximum_frequency=MAXIMUM_FREQUENCY
)

waveform_generator = bilby.gw.WaveformGenerator(
    duration=DURATION, sampling_frequency=SAMPLING_FREQUENCY,
    frequency_domain_source_model=MODEL[args.source],
    parameter_conversion=CONVERSION[args.source],
    waveform_arguments=waveform_arguments)

ifos = bilby.gw.detector.InterferometerList(interferometers)
for ifo in ifos:
    ifo.power_spectral_density = PowerSpectralDensity(
        asd_file=asd_files[ifo.name])
    ifo.maximum_frequency = MAXIMUM_FREQUENCY
    ifo.minimum_frequency = MINIMUM_FREQUENCY
    if calibration_files is not None:
        ifo.calibration_model = bilby.gw.detector.calibration.CubicSpline(
            prefix=f"recalib_{ifo.name}_",
            minimum_frequency=ifo.minimum_frequency,
            maximum_frequency=ifo.maximum_frequency,
            n_points=10,
        )
ifos.set_strain_data_from_power_spectral_densities(
    sampling_frequency=SAMPLING_FREQUENCY, duration=DURATION,
    start_time=START_TIME)

bilby.core.utils.check_directory_exists_and_if_not_mkdir(args.outdir)
bilby.core.utils.check_directory_exists_and_if_not_mkdir(args.outdir + "/no_inj")
for ifo in ifos:
    channel = f"{ifo.name}:O4MDC"
    filename = f"{args.outdir}/no_inj/{ifo.name}-O4MDC-{START_TIME}-{DURATION}.gwf"
    timeseries = TimeSeries(
        ifo.time_domain_strain, sample_rate=ifo.sampling_frequency,
        t0=ifo.start_time, channel=channel, name=channel,
    )

    timeseries.write(filename)

ifos.inject_signal(waveform_generator=waveform_generator,
                   parameters=injection_parameters,
                   raise_error=False,
                   )

meta_data = dict(args=args.__dict__, sim_version=get_git_message())

time_offset = np.random.normal(0, THALFWINDOW/10)
meta_data["trigger"] = dict(
    geocent=injection_parameters["geocent_time"] + time_offset,
    chirp_mass=injection_parameters["chirp_mass"]
)

for ifo in ifos:
    channel = f"{ifo.name}:O4MDC"
    filename = f"{args.outdir}/{ifo.name}-O4MDC-{START_TIME}-{DURATION}.gwf"
    timeseries = TimeSeries(
        ifo.time_domain_strain, sample_rate=ifo.sampling_frequency,
        t0=ifo.start_time, channel=channel, name=channel,
    )

    timeseries.write(filename)
    meta_data[ifo.name] = dict(
        psd_file=ifo.power_spectral_density.psd_file,
        asd_file=ifo.power_spectral_density.asd_file,
        channel=channel,
        filename=os.path.abspath(filename),
    )

    if args.plot:
        psd = timeseries.psd()
        fig = psd.plot()
        fig.savefig(filename.replace("gwf", "png"))

if args.blind is False:
    waveform_defaults = {
        'reference_frequency': REFERENCE_FREQUENCY,
        'waveform_approximant': args.waveform,
        'minimum_frequency': MINIMUM_FREQUENCY,
    }

    injection_full = bilby.gw.conversion._generate_all_cbc_parameters(
        injection_parameters, defaults=waveform_defaults,
        base_conversion=CONVERSION[args.source])

    for key in injection_full:
        if key != "waveform_approximant":
            injection_full[key] = float(injection_full[key])
    meta_data["injection"] = injection_full

    for ifo in ifos:
        for key in ["optimal_SNR", "matched_filter_SNR"]:
            meta_data[ifo.name][key + "_abs"] = np.abs(ifo.meta_data[key])

filename = f"{args.outdir}/meta.json"
with open(filename, 'w') as file:
    json.dump(meta_data, file, indent=4)
