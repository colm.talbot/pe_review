"""
Generate random sets of detectors for each event such that half of the events
use three detectors and the remainder use a random pair of detectors.

The split corresponds to independent duty cycles of 75%.
"""
import json
import re

import numpy as np

detectors = ["H1", "L1", "V1"]
total_events = 1000

np.random.seed(170104)
detector_strings = list(np.random.choice(
    ["H1L1", "H1V1", "L1V1", "H1L1V1"],
    total_events,
    p=(1 / 6, 1 / 6, 1 / 6, 1 / 2),
))

detector_sets = [re.findall(r"[H,L,V]1", value) for value in detector_strings]

base_directory = "/home/pe.o4/sampler-review-new/review_test_data"
bins = ["256s", "128s", "64s-nsbh", "64s", "32s", "16s", "8s", "4s", "2s", "1s"]

for ii, group in enumerate(bins):
    subset = detector_sets[ii * 100:(ii + 1) * 100]
    with open(f"{base_directory}/{group}/all_networks.json", "w") as ff:
        json.dump({jj: sub for jj, sub in enumerate(subset)}, ff, indent=2)

