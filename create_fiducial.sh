# /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py39/bin/python /home/colm.talbot/O4/pe_review/create_single_frame.py\
#         --waveform IMRPhenomXPHM --source bbh --seed 200224 --outdir /home/colm.talbot/O4/review_test_data/fiducial/XPHM-BBH\
#         --population /home/colm.talbot/O4/pe_review/prior-files/fiducial-bbh.injection --reference-time 1238175800 --disable-calibration
# 
# /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py39/bin/python /home/colm.talbot/O4/pe_review/create_single_frame.py\
#         --waveform SEOBNRv4PHM --source bbh --seed 200224 --outdir /home/colm.talbot/O4/review_test_data/fiducial/SEOB-BBH\
#         --population /home/colm.talbot/O4/pe_review/prior-files/fiducial-bbh.injection --reference-time 1238175800 --disable-calibration
# 
# /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py39/bin/python /home/colm.talbot/O4/pe_review/create_single_frame.py\
#         --waveform IMRPhenomTPHM --source bbh --seed 191105 --outdir /home/colm.talbot/O4/review_test_data/fiducial/TPHM-EXT\
#         --population /home/colm.talbot/O4/pe_review/prior-files/fiducial-extreme.injection --reference-time 1372204818 --disable-calibration

/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py39/bin/python /home/colm.talbot/O4/pe_review/create_single_frame.py\
        --waveform IMRPhenomPv2_NRTidalv2 --source bns --seed 191216 --outdir /home/colm.talbot/O4/review_test_data/fiducial/Pv2-BNS\
        --population /home/colm.talbot/O4/pe_review/prior-files/fiducial-bns.injection --reference-time 1364342418 --disable-calibration

/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py39/bin/python /home/colm.talbot/O4/pe_review/create_single_frame.py\
        --waveform TEOBResumS --source bns --seed 191216 --outdir /home/colm.talbot/O4/review_test_data/fiducial/TEOB-BNS\
        --population /home/colm.talbot/O4/pe_review/prior-files/fiducial-bns.injection --reference-time 1364342418 --disable-calibration

