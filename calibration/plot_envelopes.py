import glob

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

cal_files = glob.glob("*.txt")

fig, axes = plt.subplot_mosaic([["magnitude"], ["phase"]], figsize=(8, 6), sharex=True)

for ii, cal_file in enumerate(cal_files):
    print(cal_file)
    label = cal_file.split("/")[-1].split("_")[2]
    if "calibration" in label:
        label = "Virgo"
    cal_data = np.genfromtxt(cal_file).T

    axes["magnitude"].semilogx(cal_data[0], cal_data[1], color=f"C{ii}", label=label)
    axes["magnitude"].fill_between(cal_data[0], cal_data[3], cal_data[5], color=f"C{ii}", alpha=0.3)
    axes["phase"].semilogx(cal_data[0], cal_data[2], color=f"C{ii}", label=label)
    axes["phase"].fill_between(cal_data[0], cal_data[4], cal_data[6], color=f"C{ii}", alpha=0.3)

axes["magnitude"].legend(loc="upper right")
axes["magnitude"].set_ylabel("$\\Delta A [\\%]$")
axes["phase"].set_xlabel("Frequency [Hz]")
axes["phase"].set_ylabel("$\\Delta \\phi$ [deg]")
axes["phase"].set_xlim(10, 4096)
plt.tight_layout()
plt.savefig("uncertainties.pdf")
plt.close()

