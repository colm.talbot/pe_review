#!/usr/env python3
import os
from argparse import ArgumentParser
from pathlib import Path

import numpy as np
import pandas as pd
from scipy.signal.windows import tukey
from tqdm.auto import trange

from gwpy.detector import Channel
from gwpy.frequencyseries import FrequencySeries

from bilby.core.prior import Uniform
from bilby.core.utils import check_directory_exists_and_if_not_mkdir, logger

from bilby.gw.detector import InterferometerList, PowerSpectralDensity
from bilby.gw.prior import BBHPriorDict
from bilby.gw.waveform_generator import WaveformGenerator
from bilby_pipe.utils import convert_string_to_dict


def create_parser():
    parser = ArgumentParser(description="Create PSDs corrected for finite duration effects")
    parser.add_argument("--n-average", type=int, default=64)
    parser.add_argument("--sampling-frequency", type=float, default=4096)
    parser.add_argument("--duration", type=int, default=4)
    parser.add_argument(
        "--format",
        type=str,
        default="csv",
        help="Format to save data in, default is hdf5.",
    )
    parser.add_argument("--outdir", type=str, default=".")
    parser.add_argument(
        "--interferometers",
        default=["H1", "L1", "V1"],
        nargs="*",
        help="IFOs to make data for.",
    )
    parser.add_argument(
        "--psd-dict",
        type=str,
        default="default",
        help="PSD dictionary in bilby_pipe format. Default will use Bilby built in defaults.",
    )
    return parser


def main():
    parser = create_parser()
    args = parser.parse_args()
    check_directory_exists_and_if_not_mkdir(args.outdir)

    if args.psd_dict == "default":
        psd_dict = dict()
    else:
        psd_dict = convert_string_to_dict(args.psd_dict)


    ifos = InterferometerList(args.interferometers)
    for ifo in ifos:
        ifo.minimum_frequency = 10
        if ifo.name in psd_dict:
            method = PowerSpectralDensity.from_amplitude_spectral_density_file
            ifo.power_spectral_density = method(psd_dict[ifo.name])
        ifo.set_strain_data_from_power_spectral_density(
            duration=args.duration, sampling_frequency=args.sampling_frequency
        )
        _ = ifo.time_domain_strain

        psd = np.zeros(len(ifo.frequency_array))
        window = ifo.strain_data.time_domain_window()
        for _ in range(args.n_average):
            data, _ = ifo.power_spectral_density.get_noise_realisation(
                sampling_frequency=args.sampling_frequency,
                duration=args.duration,
            )
            windowed_data = np.fft.rfft(np.fft.irfft(data) * window)
            psd += abs(windowed_data)**2 * 4 / args.duration
        psd /= args.n_average
        frequencies = ifo.frequency_array
        np.savetxt(
            f"{args.outdir}/{ifo.name}_{args.duration}s_psd.{args.format}",
            np.array([frequencies, psd]).T,
        )
        psd = FrequencySeries(psd, frequencies=frequencies)
        # psd.write(f"{args.outdir}/{ifo.name}_{args.duration}s_psd.{args.format}")


if __name__ == "__main__":
    main()
